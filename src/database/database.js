module.exports = {
  host: '127.0.0.1',
  username: 'user',
  password: 'password',
  database: 'Mob2con',
  dialect: 'postgres',
  operatorsAliases: false,
  logging: flase,
  define:{
    timestamps: true,
    underscored: true,
    underscoredAll: true
  }
}