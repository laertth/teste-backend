module.exports = {
    up: (queryInterface, Sequelize) =>
      queryInterface.createTable('Usuario', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        nome: {
          type: Sequelize.TEXT,
          allowNull: false,
        },
        usuario: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        senha: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        rede: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }),
    down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Usuario'),
  };