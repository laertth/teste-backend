module.exports = {
    up: (queryInterface, Sequelize) =>
      queryInterface.createTable('Rede', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        nome: {
          type: Sequelize.TEXT,
          allowNull: false,
        },
        cnpj: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }),
    down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Rede'),
  };