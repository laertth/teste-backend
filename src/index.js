const express = require('express');
const bodyParser = require('body-parser');
const port = 5000;

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.get('/', (req, res) => {
    res.send('Funcionou! Teste, teste, teste...');
})

// require ('./controllers/authController')(app);
// require ('./controllers/projectcontroller')(app);
app.listen(port);