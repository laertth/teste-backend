//Verificando autorização com middlewares
const User = require('../database/migrations/user');
const authConfig = require('../config/auth.json');
const jwt = require('jsonwebtoken');
module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization
    const parts = authHeader.split('');

    if(!authHeader)
    return res.status(401).send({err: 'Não autorizado'});

    if(!parts.length === 2)
    return res.status(401).send({err: 'Token error'});

    const [schema, token] = parts

    if(!/Mob2con$/i.test(schema))
    return res.status(401).send({err: 'Formado de token invalido'});

    jwt.verify(token, authConfig.secret, (err, decoded) => {
        if (err)
        return res.status(401).send({err: 'Token invalido'})

        req.userId = decoded.id;

        return next();
    })
}