//Rotas de cadastro e autenticação
const express = require('express');
const bcript = require('bcryptjs');
const User = require('../database/migrations/user');
const jwt = require('jsonwebtoken');
const authConfig = require('../config/auth.json');

const router = express.Router();

function geradorToken(params = {}) {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400,
    }) 
}

// rota cadastro
router.post('/registrar', async (req, res) => {
    const  { email } = req.body;
    try {
        if (await User.findOne({email})) {
            return res.status(400).send({err: 'Usuario já existe!'})
        }
        const user = await User.create(req.body)
        return res.send({ 
            user,
            token: geradorToken({id:user.id})
         })
    } catch (error) {
        return error
    }
});

//rota atenticação
router.post('/autenticacao', async (req, res) => {
    const {usuario, senha} = req.body;
    const user = await User.findOne({email}).select('+senha');
    if (!user) 
        return res.status(400).send({err: 'Usuario não encontrado'})
    

    if (!senha) 
        return res.status(400).send({err: 'Senha invalida'})
        user.senha = undefined;
    

    res.send({
        user,
        token: geradorToken({id:user.id})});
})

// module.exports = app => app.user('/auth', router);