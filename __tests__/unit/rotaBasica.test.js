const request = require('supertest');
const express = require('express');
const app = express();

//declarando a variável do token em um escopo acessível por todo o conjunto de testes
let token;

beforeAll((done) => {
  request(app)
    .post('/login')
    .send({
      username: user,
      password: pw,
    })
    .end((err, response) => {
      token = response.body.token; // salvar token
      done();
    });
});

describe('GET /', () => {
  // token não enviado - resposta 401
  test('It should require authorization', () => {
    return request(app)
      .get('/')
      .then((response) => {
        expect(response.statusCode).toBe(401);
      });
  });
  // token enviado - resposta 200
  test('It responds with JSON', () => {
    return request(app)
      .get('/')
      .set('Authorization', `Bearer ${token}`)
      .then((response) => {
        expect(response.statusCode).toBe(200);
        expect(response.type).toBe('application/json');
      });
  });
});
